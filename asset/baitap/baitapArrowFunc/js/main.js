var colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let render = () => {
  colors = "";
  colorList.forEach((color) => {
    return (colors += `<button  onclick="changeColor('${color}')" class="color-button ${color}"></button>`);
  });
  document.getElementById("colorContainer").innerHTML = colors;
};
render();

let changeColor = (color) => {
  let houseElement = document.getElementById("house");
  houseElement.setAttribute("class", `house ${color}`);
};
