let tinhDiemTrungBinh = (...rest) => {
  let diemTong = 0;
  let diemTB = 0;

  for (let i = 0; i < rest.length; i++) {
    diemTong += rest[i];
    diemTB = diemTong / rest.length;
  }
  return diemTB.toFixed(2);
};
let tinhDiemK1 = () => {
  let diemToan = document.querySelector("#inpToan").value * 1;
  let diemLy = document.querySelector("#inpLy").value * 1;
  let diemHoa = document.querySelector("#inpHoa").value * 1;
  let result = tinhDiemTrungBinh(diemToan, diemLy, diemHoa);

  document.querySelector("#tbKhoi1").innerHTML = result;
};

let tinhDiemK2 = () => {
  let diemVan = document.querySelector("#inpVan").value * 1;
  let diemSu = document.querySelector("#inpSu").value * 1;
  let diemDia = document.querySelector("#inpDia").value * 1;
  let diemEnglish = document.querySelector("#inpEnglish").value * 1;
  let result = tinhDiemTrungBinh(diemVan, diemSu, diemDia, diemEnglish);

  document.querySelector("#tbKhoi2").innerHTML = result;
};
